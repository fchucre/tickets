User.create(name: 'Oracle',
  email: 'oracle@example.com',
  user_type: :admin,
  password: '123456',
  password_confirmation: '123456')

agent = User.create(name: 'Smith',
  email: 'smith@example.com',
  user_type: :agent,
  password: '123456',
  password_confirmation: '123456')

customer = User.create(name: 'Anderson',
 email: 'anderson@example.com',
 user_type: :customer,
 password: '123456',
 password_confirmation: '123456')

Ticket.create!(
  description: '1st ticket',
  status: :open,
  owner: customer)

Ticket.create!(
  description: '2nd ticket',
  status: :inprogress,
  owner: customer,
  assignee: agent)
