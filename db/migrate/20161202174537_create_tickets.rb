class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.references :owner, foreign_key: true, references: :users
      t.references :assignee, foreign_key: true, references: :users
      t.string :description
      t.string :status
      t.string :resolution
      t.datetime :closed_at

      t.timestamps
    end
  end
end
