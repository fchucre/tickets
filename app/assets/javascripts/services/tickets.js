(function() {
  'use strict';

  angular.module('api.tickets', ['ngResource'])
  .factory('Tickets', function($resource) {
    return $resource('/tickets/:id.json', {
      id: '@id'
    }, {
      start: {
        method: "POST",
        url: '/tickets/:id/start.json'
      },
      close: {
        method: "POST",
        url: '/tickets/:id/close.json'
      }
    });
  });
})();
