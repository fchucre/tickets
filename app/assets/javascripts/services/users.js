(function() {
  'use strict';

  angular.module('api.users', ['ngResource'])
  .factory('Users', function($resource) {
    return $resource('/users/:id.json', {
      id: '@id'
    }, {
      update: {
        method: 'PATCH'
      }
    });
  });
})();
