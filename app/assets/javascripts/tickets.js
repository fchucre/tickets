(function() {
  'use strict';

  angular.module('tickets', [
    'ngRoute',
    'api.tickets',
    'api.users'
  ])
  .config(function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/views/tickets.html',
        controller: 'ListTicketsCtrl'
      })
      .when('/ticket/new', {
        templateUrl: '/views/single_ticket.html',
        controller: 'TicketCtrl'
      })
      .when('/tickets/:id', {
        templateUrl: '/views/single_ticket.html',
        controller: 'TicketCtrl'
      })
      .when('/users', {
        templateUrl: '/views/users.html',
        controller: 'ListUsersCtrl'
      })
      .when('/users/new', {
        templateUrl: '/views/single_user.html',
        controller: 'UserCtrl'
      })
      .when('/users/:id', {
        templateUrl: '/views/single_user.html',
        controller: 'UserCtrl'
      })

      .otherwise({redirectTo: '/'});
    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(true);
  });
})();

