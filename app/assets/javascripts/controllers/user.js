(function() {
  'use strict';

  angular.module('tickets')
  .controller('UserCtrl', function UserCtrl($scope, Users, $routeParams, $location) {
    if ($routeParams.id && $routeParams.id!='new') {
      $scope.user = Users.get({id: $routeParams.id});
    } else {
      $scope.user = new Users();
    }

    var create = function() {
      $scope.errors = null;
      Users.save({}, {
        user: $scope.user
      }).$promise.then(function(user){
        $location.path('/users');
      }).catch(function(errors){
        $scope.errors = errors.data;
      })
    };

    var update = function() {
      $scope.errors = null;
      Users.update({id: $scope.user.id}, {
        user: $scope.user
      }).$promise.then(function(user){
        $location.path('/users');
      }).catch(function(errors){
        $scope.errors = errors.data;
      })
    }

    $scope.save = function() {
      if ($scope.user.id) {
        update();
      } else {
        create();
      }
    }

    $scope.delete = function() {
      $scope.errors = null;
      Users.delete({id: $scope.user.id})
      .$promise.then(function(user){
        $location.path('/users');
      }).catch(function(errors){
        $scope.errors = errors.data;
      })
    }

  });
})();


