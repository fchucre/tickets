(function() {
  'use strict';

  angular.module('tickets')
  .controller('TicketCtrl', function TicketCtrl($scope, Tickets, $routeParams) {
    if ($routeParams.id && $routeParams.id!='new') {
      $scope.ticket = Tickets.get({id: $routeParams.id});
    } else {
      $scope.ticket = new Tickets({
        status: 'open'
      });
    }

    $scope.start = function() {
      $scope.ticket.$start();      
    }

    $scope.close = function() {
      $scope.errors = null;
      Tickets.close({id: $scope.ticket.id}, {
        ticket: {
          resolution: $scope.ticket.resolution
        }
      }).$promise.then(function(ticket){
        $scope.ticket = ticket;
      }).catch(function(errors){
        $scope.errors = errors.data;
      })
    }

    $scope.save = function() {
      $scope.errors = null;
      Tickets.save({}, {
        ticket: {
          description: $scope.ticket.description,
          status: $scope.ticket.status
        }
      }).$promise.then(function(ticket){
        $scope.ticket = ticket;
      }).catch(function(errors){
        $scope.errors = errors.data;
      })
    }

  });
})();


