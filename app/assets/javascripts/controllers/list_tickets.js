(function() {
  'use strict';

  function getQueryParams(params) {
    var query = {}
      , filters = ['status', 'email'];
    for(var i in filters) {
      if (params[filters[i]]) {
        query[filters[i]] = params[filters[i]]
      }
    }

    return query;
  }

  angular.module('tickets')
  .controller('ListTicketsCtrl', function ListTicketsCtrl($scope, Tickets, $routeParams) {
    var query = getQueryParams($routeParams);
    $scope.tickets = Tickets.query(query);


    $scope.filter = function() {
      $scope.tickets = Tickets.query(getQueryParams($scope));
    }
  });
})();


