class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  enum user_type: {admin: 'admin', customer: 'customer', agent: 'agent'}

  def agent?
    user_type == 'agent' || admin?
  end

  def admin?
    user_type == 'admin'
  end

  def role_symbols
    [user_type.to_s.to_sym]
  end
end
