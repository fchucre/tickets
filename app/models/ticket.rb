class Ticket < ApplicationRecord
  belongs_to :owner, class_name: 'User'
  belongs_to :assignee, class_name: 'User', optional: true

  enum status: { open: 'open', inprogress: 'inprogress', closed: 'closed' }

  validates :owner, :description, :status, presence: true
  validates :assignee, presence: true, if: :need_assignee?
  validates :closed_at, :resolution, presence: true, if: :closed?

  before_validation :set_closed_at

  scope :owned_by, ->(user) {
    where(owner_id: user.id)
  }
  
  private

  def need_assignee?
    !["", "open"].include?(status.to_s)
  end

  def closed?
    status.to_s == 'closed'
  end

  def set_closed_at
    if closed? && self.closed_at.nil?
      self.closed_at = Time.now
    end
  end
end
