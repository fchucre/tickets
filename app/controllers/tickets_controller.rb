class TicketsController < ApplicationController

  def index
    if current_user.agent?
      @tickets = Ticket.where({})
    else
      @tickets = Ticket.owned_by(current_user)
    end
    if filter_params[:status]
      @tickets = @tickets.where(status: filter_params[:status])
    end
    if filter_params[:email]
      user = User.find_by_email(filter_params[:email])
      @tickets = @tickets.owned_by(user)
    end
    render json: @tickets.to_json(include: tickets_includes)
  end

  def show
    @ticket = Ticket.find id_param
    respond_to do |format|
      format.html { render 'home/main', layout: false }
      format.json { render :json => @ticket.to_json(include: tickets_includes) }
    end
  end

  def new
    @ticket = Ticket.new
    respond_to do |format|
      format.html { render 'home/main', layout: false }
      format.json { render :json => @ticket.to_json(include: tickets_includes) }
    end
  end

  def create
    @ticket = Ticket.new ticket_params.merge({
      owner: current_user,
      status: "open"
      })
    if @ticket.save
      render json: @ticket.to_json(include: tickets_includes), status: :created
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  def start
    @ticket = Ticket.find id_param
    @ticket.status = 'inprogress'
    @ticket.assignee = current_user
    if @ticket.save
      render json: @ticket.to_json(include: tickets_includes), status: :ok
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  def close
    @ticket = Ticket.find id_param
    @ticket.status = 'closed'
    @ticket.resolution = ticket_params[:resolution]
    if @ticket.save
      render json: @ticket.to_json(include: tickets_includes), status: :ok
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  private

  def id_param
    params.permit(:id)[:id]
  end

  def ticket_params
    params.require(:ticket).permit(:description, :resolution)
  end

  def filter_params
    params.permit(:status, :email)
  end

  def tickets_includes
    user_include = {only: [:id, :email, :name]}
    {
      owner: user_include,
      assignee: user_include
    }
  end
end
