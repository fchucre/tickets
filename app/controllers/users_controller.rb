class UsersController < ApplicationController

  def index
    @users = User.all
    respond_to do |format|
      format.html { render 'home/main', layout: false }
      format.json { render json: @users }
    end
  end

  def show
    @user = User.find user_id_param
    respond_to do |format|
      format.html { render 'home/main', layout: false }
      format.json { render json: @user }
    end
  end

  def new
    respond_to do |format|
      format.html { render 'home/main', layout: false }
      format.json { render json: User.new }
    end
  end

  def create
    @user = User.new user_params
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    @user = User.find user_id_param
    if @user.update_attributes user_params
      render json: @user, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find user_id_param
    @user.destroy
    render json: @user, status: :ok
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :user_type)
  end

  def user_id_param
    params.permit(:id)[:id]
  end
end
