class ApplicationController < ActionController::Base
  filter_access_to :all

  #replace declarative_authorization default behaviour
  def permission_denied
    respond_to do |format|
      format.html { redirect_to '/users/sign_in' }
      format.json { render :json => {authorization: [t('not_authorized')]}, status: 403 }
    end
  end
end
