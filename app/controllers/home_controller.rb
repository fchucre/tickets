class HomeController < ApplicationController

  def main
    if current_user
      render layout: false
    else
      redirect_to '/users/sign_in'
    end
  end
end
