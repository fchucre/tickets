describe('Users factory', function() {
  var Users
    , $httpBackend
    , users = [
      {id: 1, name: 'Jonh doe', email: 'jonhdoe@example.com'},
      {id: 2, name: 'Jane Doe', email: 'janedoe@example.com'}
    ]
    ;

  // Load our api.users module
  beforeEach(angular.mock.module('api.users'));

  // Set our injected Users factory (_Userss_) to our local Users variable
  beforeEach(inject(function(_Users_, $injector) {
    Userss = _Userss_;
    $httpBackend = $injector.get('$httpBackend');
  }));

  // A simple test to verify the Users service exists
  it('should exist', function() {
    expect(Users).toBeDefined();
  });

  // a feel test to ensure a ngResource is used
  describe('.query()', function() {
    // A simple test to verify the method all exists
    it('should exist', function() {
      expect(Users.query).toBeDefined();
    });

    it('should return a list of users', function(){
      $httpBackend.expectGET('/users.json').respond(users);

      var response = Users.query({});

      $httpBackend.flush();

      expect(response.length).toBe(2);
      expect(response[0]).toEqual(new Users(users[0]));
      expect(response[1]).toEqual(new Users(users[1]));
    })

  });

});
