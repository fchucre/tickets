describe('Tickets factory', function() {
  var Tickets
    , $httpBackend
    , tickets = [{id: 1, description: 'little desc'}, {id: 2, description: 'other little desc'}]
    ;

  // Load our api.tickets module
  beforeEach(angular.mock.module('api.tickets'));

  // Set our injected Tickets factory (_Tickets_) to our local Tickets variable
  beforeEach(inject(function(_Tickets_, $injector) {
    Tickets = _Tickets_;
    $httpBackend = $injector.get('$httpBackend');
  }));

  beforeEach(inject(function($injector) {
    $httpBackend = $injector.get('$httpBackend');
  }));

  // A simple test to verify the Tickets service exists
  it('should exist', function() {
    expect(Tickets).toBeDefined();
  });

  // a feel test to ensure a ngResource is used
  describe('.query()', function() {
    // A simple test to verify the method all exists
    it('should exist', function() {
      expect(Tickets.query).toBeDefined();
    });

    it('should return a list of tickets', function(){
      $httpBackend.expectGET('/tickets.json').respond(tickets);

      var response = Tickets.query({});
      
      $httpBackend.flush();

      expect(response.length).toBe(2);
      expect(response[0]).toEqual(new Tickets(tickets[0]));
      expect(response[1]).toEqual(new Tickets(tickets[1]));
    })

  });

  describe('.start()', function(){
    it('should exist', function() {
      expect(Tickets.start).toBeDefined();
    });

    it ('should post a start action', function(){
      var new_state = {
        status: 'inprogress'
      }
      angular.extend(new_state, tickets[0]);

      $httpBackend.expectPOST('/tickets/1/start.json').respond(new_state);

      var response = Tickets.start(tickets[0]);
      $httpBackend.flush();
      
      //angular.toJson removes $$ notation from objects
      expect(angular.toJson(response)).toEqual(angular.toJson(new_state));
    });
  })

  describe('.close()', function(){
    it('should exist', function() {
      expect(Tickets.close).toBeDefined();
    });

    it ('should post a close action', function(){
      var new_state = {
        status: 'close',
        resolution: 'my own resolution'
      }

      angular.extend(new_state, tickets[0]);

      $httpBackend.expectPOST('/tickets/1/close.json', {resolution: new_state.resolution})
      .respond(new_state);

      var response = Tickets.close({id: tickets[0].id}, {resolution: new_state.resolution});
      $httpBackend.flush();

      //angular.toJson removes $$ notation from objects
      expect(angular.toJson(response)).toEqual(angular.toJson(new_state));
    });
  })

});
