describe('ListUsersCtrl', function() {
  var Users
    , $httpBackend
    , $controller
    , users = [
      {id: 1, name: 'Jonh doe', email: 'jonhdoe@example.com'},
      {id: 2, name: 'Jane Doe', email: 'janedoe@example.com'}
    ]
    ;

  beforeEach(angular.mock.module('tickets'));

  beforeEach(inject(function(_$controller_, $injector, _Users_) {
    $controller = _$controller_;
    $httpBackend = $injector.get('$httpBackend');
    Users = _Users_;
  }));

  describe('$scope.users', function() {
    it('get all users without filters', function() {
      $httpBackend.expectGET('/users.json').respond(users);

      var $scope = {};
      var controller = $controller('ListUsersCtrl', { $scope: $scope });

      $httpBackend.flush();

      expect($scope.users.length).toBe(2);
      expect($scope.users[0]).toEqual(new Users(users[0]));
      expect($scope.users[1]).toEqual(new Users(users[1]));
    });

  });
});
