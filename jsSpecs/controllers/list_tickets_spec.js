describe('ListTicketsCtrl', function() {
  var Tickets
    , $httpBackend
    , $controller
    , owner = {id: 1, name: 'Fernando', email: 'fernando@example.com' }
    , tickets = [
        {id: 1, description: 'little desc', owner: owner}, 
        {id: 2, description: 'other little desc', status: 'inprogress'}
      ]
    ;

  beforeEach(angular.mock.module('tickets'));

  beforeEach(inject(function(_$controller_, $injector, _Tickets_) {
    $controller = _$controller_;
    $httpBackend = $injector.get('$httpBackend');
    Tickets = _Tickets_;
  }));

  describe('$scope.tickets', function() {
    it('get all tickets without filters', function() {
      $httpBackend.expectGET('/tickets.json').respond(tickets);
      
      var $scope = {};
      var controller = $controller('ListTicketsCtrl', { $scope: $scope });

      $httpBackend.flush();

      expect($scope.tickets.length).toBe(2);
      expect($scope.tickets[0]).toEqual(new Tickets(tickets[0]));
      expect($scope.tickets[1]).toEqual(new Tickets(tickets[1]));
    });

    it('should filter by status', function(){
      $httpBackend.expectGET('/tickets.json?status=inprogress').respond([tickets[1]]);
      var $scope = {};
      var $routeParams = { status: 'inprogress' };
      var controller = $controller('ListTicketsCtrl', { $scope: $scope, $routeParams: $routeParams });
      $httpBackend.flush();

      expect($scope.tickets.length).toBe(1);
      expect($scope.tickets[0]).toEqual(new Tickets(tickets[1]));
    })

    it('should filter by email', function(){
      $httpBackend.expectGET('/tickets.json?email=fernando@example.com').respond([tickets[0]]);
      var $scope = {};
      var $routeParams = { email: 'fernando@example.com' };
      var controller = $controller('ListTicketsCtrl', { $scope: $scope, $routeParams: $routeParams });
      $httpBackend.flush();

      expect($scope.tickets.length).toBe(1);
      expect($scope.tickets[0]).toEqual(new Tickets(tickets[0]));
    })
  });

  describe('$scope.filter', function() {
    it('should filter by $scopeemail', function(){
      $httpBackend.expectGET('/tickets.json').respond([tickets]);
      
      var $scope = {
          status: 'open',
          email: 'fernando@example.com'
        };
      var $routeParams = {}
      var controller = $controller('ListTicketsCtrl', { $scope: $scope, $routeParams: $routeParams });
      $httpBackend.flush();

      $httpBackend.expectGET('/tickets.json?status=open&email=fernando@example.com').respond([tickets[0]]);
      
      $scope.filter();
      $httpBackend.flush();

      expect($scope.tickets.length).toBe(1);
      expect($scope.tickets[0]).toEqual(new Tickets(tickets[0]));
    })
  });
});
