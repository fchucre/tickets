describe('TicketCtrl', function() {
  var Tickets
    , $httpBackend
    , $controller
    , owner = {id: 1, name: 'Fernando', email: 'fernando@example.com' }
    , tickets = [
        {id: 1, description: 'little desc', owner: owner}, 
        {id: 2, description: 'other little desc', status: 'inprogress'}
      ]
    ;

  beforeEach(angular.mock.module('tickets'));

  beforeEach(inject(function(_$controller_, $injector, _Tickets_) {
    $controller = _$controller_;
    $httpBackend = $injector.get('$httpBackend');
    Tickets = _Tickets_;
  }));

  describe('$scope.ticket', function() {
    it('get ticket from params', function() {
      $httpBackend.expectGET('/tickets/1.json').respond(tickets[0]);
      
      var $scope = {};
      var $routeParams = { id: 1 };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      expect(angular.toJson($scope.ticket)).toEqual(angular.toJson(tickets[0]));
    });

    it('instanciate new ticket if has no params with open status', function() {
      var $scope = {};
      var $routeParams = {};
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      expect($scope.ticket).toEqual(new Tickets({
        status: 'open'
      }));
    });
  });

  describe('$scope.start()', function() {
    it('post open in ticket resource', function() {
      $httpBackend.expectGET('/tickets/1.json').respond(tickets[0]);
      
      var $scope = {};
      var $routeParams = { id: 1 };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      var new_state = {
        status: 'inprogress',
        assignee_id: 2
      }

      angular.extend(tickets[0], new_state)

      $httpBackend.expectPOST('/tickets/1/start.json').respond(new_state);
      
      $scope.start();

      $httpBackend.flush();

      expect(angular.toJson($scope.ticket)).toEqual(angular.toJson(new_state));
    });
  });

  describe('$scope.close()', function() {
    it('post close in ticket resource', function() {
      $httpBackend.expectGET('/tickets/2.json').respond(tickets[1]);
      
      var $scope = {};
      var $routeParams = { id: 2 };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      var new_state = {
        status: 'closed',
        assignee_id: 2,
        closed_at: '2016-12-01T12:00:00-0300'
      }

      angular.extend(tickets[1], new_state)

      $httpBackend.expectPOST('/tickets/2/close.json', {ticket: {
          resolution: 'closed with success'
        }})
        .respond(new_state);
      
      $scope.ticket.resolution='closed with success';
      $scope.close();

      $httpBackend.flush();

      expect(angular.toJson($scope.ticket)).toEqual(angular.toJson(new_state));
    });

    it('should fail if post close without resolution', function() {
      $httpBackend.expectGET('/tickets/2.json').respond(tickets[1]);
      
      var $scope = {};
      var $routeParams = { id: 2 };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      var errors = {resolution: ["can't be blank"]};

      $httpBackend.expectPOST('/tickets/2/close.json', {ticket: {}})
        .respond(422, errors);
      
      $scope.close();

      $httpBackend.flush();

      expect(angular.toJson($scope.ticket)).toEqual(angular.toJson(tickets[1]));
      expect($scope.errors).toEqual(errors);
    });
  });

  describe('$scope.save()', function() {
    it('post a new ticket resource', function() {      
      var $scope = {};
      var $routeParams = { id: 'new' };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      var new_state = {
        status: 'open',
        owner_id: 2,
        closed_at: '2016-12-01T12:00:00-0300',
        description: 'my desc'
      }

      $httpBackend.expectPOST('/tickets.json', {ticket: {
          description: 'my desc',
          status: 'open'
        }})
        .respond(new_state);
      
      $scope.ticket.description='my desc';
      $scope.save();

      $httpBackend.flush();

      expect(angular.toJson($scope.ticket)).toEqual(angular.toJson(new_state));
    });

    it('should fail if post close without resolution', function() {
      var $scope = {};
      var $routeParams = { id: 'new' };
      var controller = $controller('TicketCtrl', { $scope: $scope, $routeParams: $routeParams });

      var errors = {description: ["can't be blank"]};

      $httpBackend.expectPOST('/tickets.json', {ticket: {
          status: 'open'
        }})
        .respond(422, errors);
      
      $scope.save();

      $httpBackend.flush();

      expect($scope.ticket).toEqual(new Tickets({status: 'open'}));
      expect($scope.errors).toEqual(errors);
    });
  });
});
