describe('UserCtrl', function() {
  var Users
    , $httpBackend
    , $controller
    , $location
    , users = [
      {id: 1, name: 'Jonh doe', email: 'jonhdoe@example.com'},
      {id: 2, name: 'Jane Doe', email: 'janedoe@example.com'}
    ]
    ;

  beforeEach(angular.mock.module('tickets'));

  beforeEach(inject(function(_$controller_, _$location_, $injector, _Users_) {
    $controller = _$controller_;
    $httpBackend = $injector.get('$httpBackend');
    $location = _$location_;
    Users = _Users_;
  }));

  describe('$scope.user', function() {
    it('get user from params', function() {
      $httpBackend.expectGET('/users/1.json').respond(users[0]);

      var $scope = {};
      var $routeParams = { id: 1 };
      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      expect(angular.toJson($scope.user)).toEqual(angular.toJson(users[0]));
    });

    it('instanciate new user if has no params with open status', function() {
      var $scope = {};
      var $routeParams = {};
      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams });

      expect($scope.user).toEqual(new Users({}));
    });
  });


  describe('$scope.delete()', function() {
    it('delete de current user resource', function() {
      var $scope = {};
      var $routeParams = {id: 1};

      $httpBackend.expectGET('/users/1.json')
        .respond(users[0]);

      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams, $location: $location });

      $httpBackend.flush();

      $httpBackend.expectDELETE('/users/1.json')
        .respond({});

      $scope.delete();

      $httpBackend.flush();

      expect($location.path()).toEqual("/users");
    });
  });

  describe('$scope.save()', function() {
    it('post a new User resource', function() {
      var $scope = {};
      var $routeParams = {};
      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams });

      var new_state = {
        name: 'mr anderson',
        email: 'neo@example.com',
        password: 'm4tr1x3x1sts',
        password_confirmation: 'm4tr1x3x1sts'
      }

      $httpBackend.expectPOST('/users.json', {user: new_state})
        .respond(new_state);

      angular.copy(new_state, $scope.user);

      $scope.save();

      $httpBackend.flush();

      expect($location.path()).toEqual("/users");
    });

    it('patch a exiting user resource', function() {
      var $scope = {};
      var $routeParams = {id: 1};

      $httpBackend.expectGET('/users/1.json')
        .respond(users[0]);


      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams });

      $httpBackend.flush();

      var new_state = {
        name: 'Neo',
        id: $scope.user.id,
        email: $scope.user.email
      }

      $httpBackend.expectPATCH('/users/1.json', {user: new_state})
        .respond(new_state);

      $scope.user.name = 'Neo';

      $scope.save();

      $httpBackend.flush();
      expect($location.path()).toEqual("/users");
    });

    it('should add errors in $scope.errors if request fail', function() {
      var $scope = {};
      var $routeParams = {  };
      var controller = $controller('UserCtrl', { $scope: $scope, $routeParams: $routeParams });

      var errors = {description: ["can't be blank"]};

      $httpBackend.expectPOST('/users.json', {user: {
          name: 'my name'
        }})
        .respond(422, errors);

      $scope.user.name = 'my name';

      $scope.save();

      $httpBackend.flush();

      expect($scope.user).toEqual(new Users({name: 'my name'}));
      expect($scope.errors).toEqual(errors);
    });
  });

});
