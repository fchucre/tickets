describe('Testing routes', function() {
  beforeEach(module('tickets'));

  var location, route, rootScope, httpBackend;

    beforeEach(inject(function() {
    }));

  beforeEach(inject(function( _$location_, _$route_, _$rootScope_, _$httpBackend_) {
    location = _$location_;
    route = _$route_;
    rootScope = _$rootScope_;
    httpBackend = _$httpBackend_
  }));

  describe('default route', function() {
    it('should load the tickets list page on successful load of /', function() {
      httpBackend.expectGET('/views/tickets.html').respond(200);
      location.path('/');
      rootScope.$digest();
      expect(route.current.controller).toBe('ListTicketsCtrl')
    });
  });

  describe('show ticket route', function() {
    it('should load the a single tickets page on successful load of /tickets/1', function() {
      httpBackend.expectGET('/views/single_ticket.html').respond(200);
      location.path('/tickets/1');
      rootScope.$digest();
      expect(route.current.controller).toBe('TicketCtrl')
    });
  });

  describe('show new ticket route', function() {
    it('should load the a single tickets page on successful load of /tickets/new', function() {
      httpBackend.expectGET('/views/single_ticket.html').respond(200);
      location.path('/tickets/new');
      rootScope.$digest();
      expect(route.current.controller).toBe('TicketCtrl')
    });
  });


  describe('show users', function() {
    it('should load the users list page on successful load of /users', function() {
      httpBackend.expectGET('/views/users.html').respond(200);
      location.path('/users');
      rootScope.$digest();
      expect(route.current.controller).toBe('ListUsersCtrl')
    });
  });

  describe('show a user route', function() {
    it('should load the a single user page on successful load of /users/1', function() {
      httpBackend.expectGET('/views/single_user.html').respond(200);
      location.path('/users/1');
      rootScope.$digest();
      expect(route.current.controller).toBe('UserCtrl')
    });
  });

  describe('show new user route', function() {
    it('should load the a single user page on successful load of /users/new', function() {
      httpBackend.expectGET('/views/single_user.html').respond(200);
      location.path('/users/new');
      rootScope.$digest();
      expect(route.current.controller).toBe('UserCtrl')
    });
  });
});
