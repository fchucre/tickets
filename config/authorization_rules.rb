# -*- encoding : utf-8 -*-

authorization do
  role :customer do
    has_permission_on :home, to: [:main]
    has_permission_on :tickets, to: [:create, :index, :show]
    has_permission_on :devise_sessions, :to=>[:new, :create, :destroy]
  end

  role :agent do
    has_permission_on :home, to: [:main]
    has_permission_on :tickets, to: [:start, :close, :index, :show]
    has_permission_on :devise_sessions, :to=>[:new, :create, :destroy]
  end

  role :admin do
    includes :customer
    includes :agent
    has_permission_on :users, to: [:index, :create, :update, :destroy, :show, :new]
  end

  role :guest do
    has_permission_on :home, to: [:main]
    has_permission_on :devise_sessions, :to=>[:new, :create]
    has_permission_on :registrations, :to=>[:new, :create]
  end
end
