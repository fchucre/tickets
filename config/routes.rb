Rails.application.routes.draw do

  resources :tickets do
    member do
      post :start
      post :close
    end
  end

  #order matters here, devise create a lot of routes for /users
  devise_for :users, controllers: { registrations: 'registrations' }

  resources :users

  root to: 'home#main'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
