// Karma configuration
// Generated on Mon Dec 05 2016 10:17:25 GMT-0200 (BRST)

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
        //app dependencies
        './node_modules/angular/angular.js',
        './node_modules/angular-resource/angular-resource.js',
        './node_modules/angular-route/angular-route.js',

        //mock dependencies
        './node_modules/angular-mocks/angular-mocks.js',

        //app files
        './app/assets/javascripts/services/tickets.js',
        './app/assets/javascripts/services/users.js',
        './app/assets/javascripts/tickets.js',
        './app/assets/javascripts/controllers/list_tickets.js',
        './app/assets/javascripts/controllers/list_users.js',
        './app/assets/javascripts/controllers/ticket.js',
        './app/assets/javascripts/controllers/user.js',

        //spec files
        './jsSpecs/services/tickets_spec.js',
        './jsSpecs/controllers/list_users_spec.js',
        './jsSpecs/controllers/list_tickets_spec.js',
        './jsSpecs/controllers/ticket_spec.js',
        './jsSpecs/controllers/user_spec.js',
        './jsSpecs/route_spec.js'
    ],
    exclude: [
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
