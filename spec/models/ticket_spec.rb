require 'rails_helper'

RSpec.describe Ticket, :type => :model do

  describe "#initial state" do
    let(:ticket) {
      Ticket.new
    }

    let(:valid_attributes) { {
      owner: build(:customer),
      status: :open,
      description: 'a minimal of description'
    } }

    it 'empty should fail' do
      expect(ticket.save).to be false
    end

    it 'success if has valid_attributes' do
      ticket.attributes= valid_attributes
      expect(ticket.save).to be true
    end

  end

  describe "assign to an agent" do
    let(:ticket) { build(:open_ticket) }

    it 'fail if has no assign' do
      ticket.status = :inprogress
      expect(ticket.save).to eq false
    end

    it 'save if has an agent' do
      ticket.status = :inprogress
      ticket.assignee = build(:agent)
      expect(ticket.save).to eq true
    end
  end

  describe "close" do
    let(:ticket) { build(:inprogress_ticket) }

    it 'should fail without a resolution' do
      ticket.status = :closed
      expect(ticket.save).to eq false
    end

    it 'should success with resolution' do
      ticket.status = :closed
      ticket.resolution = 'a little resolution'
      result = ticket.save
      expect(result).to eq true
      expect(ticket.closed_at).not_to eq nil
    end
  end

end
