FactoryGirl.define do
  factory :customer, class: User do
    name "Jonh Doe"
    sequence :email do |n|
      "email#{n}@example.com"
    end
    password '123456'
    password_confirmation '123456'
    user_type 'customer'
  end
  
  factory :agent, class: User do
    name "Smith"
    sequence :email do |n|
      "smith#{n}@example.com"
    end
    password '123456'
    password_confirmation '123456'
    user_type 'agent'
  end

  factory :admin, class: User do
    name "Oracle"
    sequence :email do |n|
      "Oracle#{n}@example.com"
    end
    password '123456'
    password_confirmation '123456'
    user_type 'admin'
  end
end
