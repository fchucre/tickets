FactoryGirl.define do
  factory :open_ticket, class: Ticket do
    association :owner, factory: :customer, strategy: :build
    status "open"
    description "a little description"
  end
  factory :inprogress_ticket, class: Ticket do
    association :owner, factory: :customer, strategy: :build
    association :assignee, factory: :agent, strategy: :build
    status "inprogress"
    description "a little description"
  end
end
