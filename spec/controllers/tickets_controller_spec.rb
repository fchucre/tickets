require 'rails_helper'

RSpec.describe TicketsController, :type => :controller do
  let(:tickets_includes) {
    user_include = {only: [:id, :email, :name]} 
    {
      owner: user_include, 
      assignee: user_include
    }
  }
  context "customer" do
    let(:user) { create(:customer) }
    before :each do
      sign_in user
    end

    describe "#index" do
      let(:other_customer) { create(:customer, name: 'Jane Roe') }
      let(:ticket1) { create(:open_ticket, owner: user) }
      let(:ticket2) { create(:inprogress_ticket, owner: user) }
      let(:ticket3) { create(:open_ticket, owner: other_customer) }

      before :each do
        #let create in database
        @tickets = [ticket1, ticket2, ticket3]
      end

      it "should get all yours tickets" do
        get :index
        expect(response.body).to eq [ticket1, ticket2].to_json(include: tickets_includes)
      end
    end

    describe "#post" do
      let (:valid_attribute) { {
        description: 'a little description'
      } }

      it 'should create with 201' do
        post :create, params: { ticket: valid_attribute }
        expect(response).to have_http_status(:created)
        ticket = JSON(response.body)
        expect(ticket["id"]).not_to be nil
        expect(ticket["description"]).to eq valid_attribute[:description]
        expect(ticket["status"]).to eq "open"       
      end

      it 'should not proccess entity with no description' do
        post :create, params: { ticket: {status: 'open'} }
        expect(response).to have_http_status(:unprocessable_entity)        
      end
    end

    describe "#start" do 
      let(:ticket) { create(:open_ticket) }
      
      it 'should to be forbidden' do
        post :start, params: {id: ticket.id, format: :json}
        expect(response).to have_http_status(403)
      end
    end

    describe "#close" do 
      let(:ticket) { create(:open_ticket) }
      
      it 'should to be forbidden' do
        post :start, params: {id: ticket.id, format: :json}
        expect(response).to have_http_status(403)
      end
    end

  end

  context "customer" do
    let(:user) { create(:agent) }
    before :each do
      sign_in user
    end

    describe "#index" do
      let(:first_customer) { create(:customer, name: 'Jane Roe') }
      let(:other_customer) { create(:customer, name: 'Jane Roe') }
      let(:ticket1) { create(:open_ticket, owner: first_customer) }
      let(:ticket2) { create(:inprogress_ticket, owner: first_customer) }
      let(:ticket3) { create(:open_ticket, owner: other_customer) }

      before :each do
        #let create in database
        @tickets = [ticket1, ticket2, ticket3]
      end

      it "should get all yours tickets" do
        get :index
        expect(response.body).to eq [ticket1, ticket2, ticket3].to_json(include: tickets_includes)
      end

      it "should find by status" do
        get :index, params: {status: 'open'}
        expect(response.body).to eq [ticket1, ticket3].to_json(include: tickets_includes)
      end

      it "should find by owner email" do
        get :index, params: {email: first_customer.email}
        expect(response.body).to eq [ticket1, ticket2].to_json(include: tickets_includes)
      end
    end

    describe "#start" do 
      let(:ticket) { create(:open_ticket) }
      
      it 'should update ticket' do
        post :start, params: {id: ticket.id}
        expect(response).to have_http_status(:success)
        ticket = JSON(response.body)
        expect(ticket["status"]).to eq "inprogress"
        expect(ticket["assignee_id"]).to eq user.id
      end
    end

    describe "#close" do 
      let(:ticket) { create(:inprogress_ticket) }
      let(:close_attributes) { {
        resolution: 'a quick resolution'
      } }

      it 'should update ticket' do
        post :close, params: {id: ticket.id, ticket: close_attributes }

        expect(response).to have_http_status(:success)
        ticket = JSON(response.body)
        expect(ticket["status"]).to eq "closed"
        expect(ticket["resolution"]).to eq close_attributes[:resolution]
        expect(ticket["closed_at"]).not_to eq nil
      end
    end
  end
end
