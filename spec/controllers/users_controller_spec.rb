require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  context "admin" do
    let(:admin) { create(:admin) }
    let(:agent) { create(:agent) }
    let(:customer) { create(:customer) }
    let(:valid_attributes) {
      {
        name: 'Jonh doe',
        email: 'jonhdoe@example.com',
        password: '123456',
        password_confirmation: '123456',
        user_type: 'customer'
      }
    }
    before :each do
      sign_in admin
      #let create in database
      @users = [admin, agent, customer]
    end

    describe "#index" do
      it 'should list all users' do
        get :index
        expect(response.body).to eq @users.to_json
      end
    end

    describe "#create" do
      it 'should create user' do
        expect {
          post :create, params: {user: valid_attributes}
        }.to change{User.count}.by(1)

        expect(response).to have_http_status(:created)
        expect(response.body).to eq User.find_by_email(valid_attributes[:email]).to_json
      end

      it 'should not create user with invalid attributes' do
        expect {
          post :create, params: {user: {name: 'short one'}}
        }.to change{User.count}.by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end


    describe "#update" do
      let(:user) {create(:customer)}

      it 'should change user' do
        #create user before use it
        user.reload

        expect {
          put :update, params: {id: user.id, user: {user_type: :admin}}
        }.to change{User.count}.by(0)

        user.reload

        expect(response).to have_http_status(:success)
        expect(user.user_type).to eq "admin"
      end

      it 'should not update user with invalid attributes' do
        #create user before use it
        user.reload

        expect {
          put :update, params: {id: user.id, user: {email: 'invalid_one'}}
        }.to change{User.count}.by(0)

        expect(response).to have_http_status(:unprocessable_entity)
        user.reload
        expect(user.email).not_to eq 'invalid_one'
      end
    end

    describe "#destroy" do
      let(:user) {create(:customer)}

      it 'should drop user' do
        #create user before use it
        user.reload

        expect {
          delete :destroy, params: {id: user.id}
        }.to change{User.count}.by(-1)

        expect(response).to have_http_status(:success)
        expect(User.find_by_id(user.id)).to eq nil
      end
    end
  end

  context "customer" do
    let(:customer) { create(:customer) }

    before :each do
      sign_in customer
    end

    describe "forbidden methods" do
      it 'should forbidden get #index' do
        get :index, params: { format: :json }
        expect(response).to have_http_status(403)
      end

      it 'should forbidden post #create' do
        post :create, params: { format: :json }
        expect(response).to have_http_status(403)
      end

      it 'should forbidden post #update' do
        put :update, params: { id: 1, format: :json }
        expect(response).to have_http_status(403)
      end

      it 'should forbidden delete #destroy' do
        delete :destroy, params: { id: 1, format: :json }
        expect(response).to have_http_status(403)
      end
    end
  end
end
